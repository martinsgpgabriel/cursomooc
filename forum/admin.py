from django.contrib import admin
from .models import Respostas, Topicos

# Register your models here.
admin.site.register(Topicos)
admin.site.register(Respostas)
